define("frmStart", function() {
    return function(controller) {
        function addWidgetsfrmStart() {
            this.setDefaultUnit(kony.flex.DP);
            var txtLogo = new kony.ui.RichText({
                "centerX": "50.03%",
                "centerY": "24.89%",
                "height": "100dp",
                "id": "txtLogo",
                "isVisible": true,
                "left": "85dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0ic3e733e52c445",
                "text": "Cinema",
                "top": "177dp",
                "width": "101.85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtTagLine = new kony.ui.RichText({
                "height": "77dp",
                "id": "txtTagLine",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0aed759b9827145",
                "text": "Here can be our tagline !",
                "top": "292dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSignUp = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignUp",
                "isVisible": true,
                "left": "61dp",
                "skin": "CopydefBtnNormal0a8580edbe90140",
                "text": "SIGN UP",
                "top": "504dp",
                "width": "154dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSignIn = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignIn",
                "isVisible": true,
                "left": "219dp",
                "skin": "CopydefBtnNormal0d8b289e539774a",
                "text": "SING IN",
                "top": "504dp",
                "width": "154dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            this.add(txtLogo, txtTagLine, btnSignUp, btnSignIn);
        };
        return [{
            "addWidgets": addWidgetsfrmStart,
            "enabledForIdleTimeout": false,
            "id": "frmStart",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0ga83c868cb3f47"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});