define("CopyFBox0fc526bff657f40", function() {
    return function(controller) {
        CopyFBox0fc526bff657f40 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "55%",
            "id": "CopyFBox0fc526bff657f40",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        CopyFBox0fc526bff657f40.setDefaultUnit(kony.flex.DP);
        var imgCinema = new kony.ui.Image2({
            "centerY": "17.39%",
            "height": "133dp",
            "id": "imgCinema",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "-1dp",
            "width": "415dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var txtCinemaTitle = new kony.ui.RichText({
            "centerY": "37.39%",
            "height": "54dp",
            "id": "txtCinemaTitle",
            "isVisible": true,
            "left": "117dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0df40c50e9e3f4b",
            "text": "RichText",
            "top": "19dp",
            "width": "43.64%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var listWhen = new kony.ui.ListBox({
            "centerX": "49.97%",
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "listWhen",
            "isVisible": true,
            "left": "85dp",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "CopydefListBoxNormal0a7f4444a09c442",
            "top": "219dp",
            "width": "260dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {
            "applySkinsToPopup": true,
            "dropDownImage": "listboxarw.png",
            "placeholder": "TODAY",
            "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
        });
        var txtTime = new kony.ui.RichText({
            "centerX": "50.00%",
            "height": "196dp",
            "id": "txtTime",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0b256540b44c641",
            "text": "RichText",
            "top": "277dp",
            "width": "92%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        CopyFBox0fc526bff657f40.add(imgCinema, txtCinemaTitle, listWhen, txtTime);
        return CopyFBox0fc526bff657f40;
    }
})