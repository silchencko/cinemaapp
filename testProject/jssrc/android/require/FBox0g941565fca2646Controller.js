define(function(){
	var controller = require("userFBox0g941565fca2646Controller");
	var controllerActions = ["FBox0g941565fca2646ControllerActions"];

	for(var i = 0; i < controllerActions.length; i++){
		var actions = require(controllerActions[i]);
		for(var key in actions){
			controller[key] = actions[key];
		}
	}

	return controller;
})