define(function(){
	var controller = require("userCopyFBox0fc526bff657f40Controller");
	var controllerActions = ["CopyFBox0fc526bff657f40ControllerActions"];

	for(var i = 0; i < controllerActions.length; i++){
		var actions = require(controllerActions[i]);
		for(var key in actions){
			controller[key] = actions[key];
		}
	}

	return controller;
})