define("frmSignIn", function() {
    return function(controller) {
        function addWidgetsfrmSignIn() {
            this.setDefaultUnit(kony.flex.DP);
            var txtSignIn = new kony.ui.RichText({
                "centerX": "50.00%",
                "height": "30dp",
                "id": "txtSignIn",
                "isVisible": true,
                "left": "158dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0e7b69feb74d940",
                "text": "SIGN IN",
                "top": "123dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtEnterData = new kony.ui.RichText({
                "centerX": "50.90%",
                "height": "23dp",
                "id": "txtEnterData",
                "isVisible": true,
                "left": "123dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0bfc7eff440604f",
                "text": "enter your data",
                "top": "162dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fldLoginReg = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "CopydefTextBoxFocus0c031bbc4fdb545",
                "height": "40dp",
                "id": "fldLoginReg",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "71dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0fa9137e1fa5c4b",
                "text": "E mail",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "200dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "defTextBoxPlaceholder",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var fldPasswordReg = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "CopydefTextBoxFocus0bf8ad9c7cde94a",
                "height": "40dp",
                "id": "fldPasswordReg",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "75dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0jf097d2ae4494f",
                "text": "Password",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "254dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "defTextBoxPlaceholder",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var txtForgtPassword = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtForgtPassword",
                "isVisible": true,
                "left": "220dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0j5d55759fd0341",
                "text": "Forgot Password?",
                "top": "304dp",
                "width": "30.48%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSignIn = new kony.ui.Button({
                "centerX": "49.97%",
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignIn",
                "isVisible": true,
                "left": "69dp",
                "skin": "CopydefBtnNormal0f5d5cf40af6e48",
                "text": "SIGN IN",
                "top": "379dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtOr = new kony.ui.RichText({
                "centerX": 211,
                "height": "27dp",
                "id": "txtOr",
                "isVisible": true,
                "left": "167dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0cfdf0ab332a349",
                "text": "OR",
                "top": "440dp",
                "width": "10.19%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSignGoogle = new kony.ui.Button({
                "centerX": "50.00%",
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignGoogle",
                "isVisible": true,
                "left": "73dp",
                "skin": "CopydefBtnNormal0gdb63fc883734f",
                "text": "Sign in with Google",
                "top": "481dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSignFacebook = new kony.ui.Button({
                "centerX": "50.03%",
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignFacebook",
                "isVisible": true,
                "left": "73dp",
                "skin": "CopydefBtnNormal0hf0cd9cea0014d",
                "text": "Sign in with Facebook",
                "top": "542dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtNewHere = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtNewHere",
                "isVisible": true,
                "left": "129dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0a97a291741874d",
                "text": "New here?",
                "top": "612dp",
                "width": "19.07%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtSingUp = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtSingUp",
                "isVisible": true,
                "left": "208dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0b933a00bfcb04f",
                "text": "Sing Up",
                "top": "612dp",
                "width": "13.89%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var chkRememberMe = new kony.ui.CheckBoxGroup({
                "height": "35dp",
                "id": "chkRememberMe",
                "isVisible": true,
                "left": "69dp",
                "masterData": [
                    ["cbg1", "Remember me"]
                ],
                "selectedKeyValues": [
                    ["cbg1", "Remember me"]
                ],
                "selectedKeys": ["cbg1"],
                "skin": "CopyslCheckBoxGroup0c977d52f5a404a",
                "top": "304dp",
                "width": "36.11%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            this.add(txtSignIn, txtEnterData, fldLoginReg, fldPasswordReg, txtForgtPassword, btnSignIn, txtOr, btnSignGoogle, btnSignFacebook, txtNewHere, txtSingUp, chkRememberMe);
        };
        return [{
            "addWidgets": addWidgetsfrmSignIn,
            "enabledForIdleTimeout": false,
            "id": "frmSignIn",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0gc220e9149db4e"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});