define("frmCatalog", function() {
    return function(controller) {
        function addWidgetsfrmCatalog() {
            this.setDefaultUnit(kony.flex.DP);
            var contMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "712dp",
                "horizontalScrollIndicator": true,
                "id": "contMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "-4dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            contMain.setDefaultUnit(kony.flex.DP);
            var listChoose = new kony.ui.ListBox({
                "centerX": "15.74%",
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "listChoose",
                "isVisible": true,
                "left": "74dp",
                "masterData": [
                    ["lb1", "Dnipro"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "selectedKey": "lb1",
                "selectedKeyValue": ["lb1", "Dnipro"],
                "skin": "CopydefListBoxNormal0je87d4ef45c744",
                "top": "8dp",
                "width": "119dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "applySkinsToPopup": true,
                "dropDownImage": "listboxarw.png",
                "placeholder": "Please Select",
                "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
            });
            var tabFilmCinema = new kony.ui.TabPane({
                "activeSkin": "CopytabCanvas0a25def49549c42",
                "activeTabs": [0],
                "height": "650dp",
                "id": "tabFilmCinema",
                "inactiveSkin": "CopytabCanvasInactive0g99eda588d6444",
                "isVisible": true,
                "layoutType": constants.CONTAINER_LAYOUT_BOX,
                "left": "0dp",
                "top": "65dp",
                "viewConfig": {
                    "collapsibleViewConfig": {
                        "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                        "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                        "toggleTabs": false
                    },
                    "pageViewConfig": {
                        "needPageIndicator": true
                    },
                    "tabViewConfig": {
                        "headerPosition": constants.TAB_HEADER_POSITION_TOP,
                        "image1": "tableftarrow.png",
                        "image2": "tabrightarrow.png"
                    },
                },
                "viewType": constants.TABPANE_VIEW_TYPE_TABVIEW,
                "width": "100%",
                "zIndex": 1
            }, {
                "layoutType": constants.CONTAINER_LAYOUT_BOX,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "tabHeaderHeight": 64
            });
            var subTabFilms = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "subTabFilms",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "orientation": constants.BOX_LAYOUT_VERTICAL,
                "skin": "CopyslTab0ac9903e7c7344c",
                "tabName": "FIlms",
                "width": "100%"
            }, {
                "layoutType": kony.flex.FLOW_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            subTabFilms.setDefaultUnit(kony.flex.PERCENTAGE);
            kony.mvc.registry.add('FBox0cb546b8d7b4f41', 'FBox0cb546b8d7b4f41', 'FBox0cb546b8d7b4f41Controller');
            var sgmFilmCollection = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "btnScreening": "Screenings",
                    "imgFilm": "film1.png",
                    "txtFilm": "Богемная рапсодия",
                    "txtGenre": "Biographical | Drama"
                }, {
                    "btnScreening": "Screenings",
                    "imgFilm": "film2.png",
                    "txtFilm": "Хеллоуин",
                    "txtGenre": "Horror | Thriller"
                }, {
                    "btnScreening": "Screenings",
                    "imgFilm": "film3.png",
                    "txtFilm": "DZIDZIO: Первый раз",
                    "txtGenre": " Comedy"
                }, {
                    "btnScreening": "Screenings",
                    "imgFilm": "film4.png",
                    "txtFilm": "Экстаз",
                    "txtGenre": "Musical | Thriller"
                }, {
                    "btnScreening": "Screenings",
                    "imgFilm": "film5.png",
                    "txtFilm": "Страшилки 2: Призраки Хэллоуина",
                    "txtGenre": " Adventure | Fantasy"
                }, {
                    "btnScreening": "Screenings",
                    "imgFilm": "film6.png",
                    "txtFilm": "Питер Пен: В поисках волшебной книги",
                    "txtGenre": " Adventure | Cartoon"
                }, {
                    "btnScreening": "Screenings",
                    "imgFilm": "film7.png",
                    "txtFilm": "Я все еще вижу тебя",
                    "txtGenre": "Adventure | Thriller"
                }, {
                    "btnScreening": "Screenings",
                    "imgFilm": "film8.png",
                    "txtFilm": "Плохие времена в «Эль Рояле»",
                    "txtGenre": "Adventure | Thriller"
                }],
                "groupCells": false,
                "height": "562dp",
                "id": "sgmFilmCollection",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0j3a0861455d448",
                "rowTemplate": "FBox0cb546b8d7b4f41",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646400",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "3dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnScreening": "btnScreening",
                    "imgFilm": "imgFilm",
                    "txtFilm": "txtFilm",
                    "txtGenre": "txtGenre"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            subTabFilms.add(sgmFilmCollection);
            tabFilmCinema.addTab("subTabFilms", "FIlms", null, subTabFilms, null);
            var subTabCinema = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "150%",
                "id": "subTabCinema",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "orientation": constants.BOX_LAYOUT_VERTICAL,
                "skin": "CopyslTab0d1b544c0ad904b",
                "tabName": "Cinemas",
                "width": "100%"
            }, {
                "layoutType": kony.flex.FREE_FORM,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            subTabCinema.setDefaultUnit(kony.flex.DP);
            kony.mvc.registry.add('FBox0g941565fca2646', 'FBox0g941565fca2646', 'FBox0g941565fca2646Controller');
            var sgmCinemaCollection = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "Button0c8ae0f929d5f49": "Screenings",
                    "Image0jea9896d32d847": "mostkino.png",
                    "RichText0b1f0d17dc5f247": "Мост Кино"
                }, {
                    "Button0c8ae0f929d5f49": "Screenings",
                    "Image0jea9896d32d847": "multiplex.png",
                    "RichText0b1f0d17dc5f247": "Multiplex Караван"
                }, {
                    "Button0c8ae0f929d5f49": "Screenings",
                    "Image0jea9896d32d847": "multiplex.png",
                    "RichText0b1f0d17dc5f247": "Multiplex Даффи"
                }, {
                    "Button0c8ae0f929d5f49": "Screenings",
                    "Image0jea9896d32d847": "pravdakino.png",
                    "RichText0b1f0d17dc5f247": "Правда Кино"
                }],
                "groupCells": false,
                "height": "600dp",
                "id": "sgmCinemaCollection",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0d4e956af451647",
                "rowTemplate": "FBox0g941565fca2646",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "4dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "Button0c8ae0f929d5f49": "Button0c8ae0f929d5f49",
                    "Image0jea9896d32d847": "Image0jea9896d32d847",
                    "RichText0b1f0d17dc5f247": "RichText0b1f0d17dc5f247"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            subTabCinema.add(sgmCinemaCollection);
            tabFilmCinema.addTab("subTabCinema", "Cinemas", null, subTabCinema, null);
            contMain.add(listChoose, tabFilmCinema);
            this.add(contMain);
        };
        return [{
            "addWidgets": addWidgetsfrmCatalog,
            "enabledForIdleTimeout": false,
            "id": "frmCatalog",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0d7ccacdea68e46"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});