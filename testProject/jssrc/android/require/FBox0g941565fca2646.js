define("FBox0g941565fca2646", function() {
    return function(controller) {
        FBox0g941565fca2646 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "22%",
            "id": "FBox0g941565fca2646",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        FBox0g941565fca2646.setDefaultUnit(kony.flex.DP);
        var Image0jea9896d32d847 = new kony.ui.Image2({
            "centerY": "50%",
            "height": "127dp",
            "id": "Image0jea9896d32d847",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "-1dp",
            "width": "150dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var RichText0b1f0d17dc5f247 = new kony.ui.RichText({
            "centerY": "50.10%",
            "height": "54dp",
            "id": "RichText0b1f0d17dc5f247",
            "isVisible": true,
            "left": "150dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0df40c50e9e3f4b",
            "text": "RichText",
            "top": "19dp",
            "width": "43.64%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var Button0c8ae0f929d5f49 = new kony.ui.Button({
            "centerY": "50.10%",
            "focusSkin": "defBtnFocus",
            "height": "50dp",
            "id": "Button0c8ae0f929d5f49",
            "isVisible": true,
            "left": "327dp",
            "skin": "CopydefBtnNormal0eb74babcd9fe4f",
            "text": "Button",
            "top": "19dp",
            "width": "81dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "hExpand": true,
            "margin": [6, 6, 6, 6],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        FBox0g941565fca2646.add(Image0jea9896d32d847, RichText0b1f0d17dc5f247, Button0c8ae0f929d5f49);
        return FBox0g941565fca2646;
    }
})