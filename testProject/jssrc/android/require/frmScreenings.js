define("frmScreenings", function() {
    return function(controller) {
        function addWidgetsfrmScreenings() {
            this.setDefaultUnit(kony.flex.DP);
            kony.mvc.registry.add('CopyFBox0fc526bff657f40', 'CopyFBox0fc526bff657f40', 'CopyFBox0fc526bff657f40Controller');
            var sgmCinemaCollection = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgCinema": "mostkino.png",
                    "listWhen": {
                        "masterData": [
                            ["lb1", "TODAY"],
                            ["lb2", "TOMORROW"],
                            ["lb3", "03.11.18"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeyValue": ["lb1", "TODAY"]
                    },
                    "txtCinemaTitle": "Мост Кино",
                    "txtTime": "14:10, 16:20, 18:30, 20:40, 23:30"
                }, {
                    "imgCinema": "multiplex.png",
                    "listWhen": {
                        "masterData": [
                            ["lb1", "TODAY"],
                            ["lb2", "TOMORROW"],
                            ["lb3", "03.11.18"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeyValue": ["lb1", "TODAY"]
                    },
                    "txtCinemaTitle": "Multiplex Караван",
                    "txtTime": "14:10, 16:20, 18:30, 20:40, 23:30"
                }, {
                    "imgCinema": "multiplex.png",
                    "listWhen": {
                        "masterData": [
                            ["lb1", "TODAY"],
                            ["lb2", "TOMORROW"],
                            ["lb3", "03.11.18"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeyValue": ["lb1", "TODAY"]
                    },
                    "txtCinemaTitle": "Multiplex Даффи",
                    "txtTime": "14:10, 16:20, 18:30, 20:40, 23:30"
                }, {
                    "imgCinema": "pravdakino.png",
                    "listWhen": {
                        "masterData": [
                            ["lb1", "TODAY"],
                            ["lb2", "TOMORROW"],
                            ["lb3", "03.11.18"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeyValue": ["lb1", "TODAY"]
                    },
                    "txtCinemaTitle": "Правда Кино",
                    "txtTime": "14:10, 16:20, 18:30, 20:40, 23:30"
                }],
                "groupCells": false,
                "height": "885dp",
                "id": "sgmCinemaCollection",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0d4e956af451647",
                "rowTemplate": "CopyFBox0fc526bff657f40",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "112dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "imgCinema": "imgCinema",
                    "listWhen": "listWhen",
                    "txtCinemaTitle": "txtCinemaTitle",
                    "txtTime": "txtTime"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtChosenFilmScrTitle = new kony.ui.RichText({
                "centerX": "64.97%",
                "height": "69dp",
                "id": "txtChosenFilmScrTitle",
                "isVisible": true,
                "left": "208dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0jf8b2561294749",
                "text": "Хеллоуин",
                "top": "21dp",
                "width": "50.93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgChosenFilmScr = new kony.ui.Image2({
                "height": "112dp",
                "id": "imgChosenFilmScr",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "film2.png",
                "top": "0dp",
                "width": "127dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            this.add(sgmCinemaCollection, txtChosenFilmScrTitle, imgChosenFilmScr);
        };
        return [{
            "addWidgets": addWidgetsfrmScreenings,
            "enabledForIdleTimeout": false,
            "id": "frmScreenings",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0ef34a85d517944"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});