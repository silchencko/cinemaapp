define("FBox0b2e37808444447", function() {
    return function(controller) {
        FBox0b2e37808444447 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "500dp",
            "id": "FBox0b2e37808444447",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        FBox0b2e37808444447.setDefaultUnit(kony.flex.DP);
        var imgChosenFilm = new kony.ui.Image2({
            "height": "208dp",
            "id": "imgChosenFilm",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "5dp",
            "width": "415dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        FBox0b2e37808444447.add(imgChosenFilm);
        return FBox0b2e37808444447;
    }
})