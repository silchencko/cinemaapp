define(function(){
	var controller = require("userFBox0cb546b8d7b4f41Controller");
	var controllerActions = ["FBox0cb546b8d7b4f41ControllerActions"];

	for(var i = 0; i < controllerActions.length; i++){
		var actions = require(controllerActions[i]);
		for(var key in actions){
			controller[key] = actions[key];
		}
	}

	return controller;
})