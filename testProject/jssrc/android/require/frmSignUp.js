define("frmSignUp", function() {
    return function(controller) {
        function addWidgetsfrmSignUp() {
            this.setDefaultUnit(kony.flex.DP);
            var fldPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50.03%",
                "focusSkin": "CopydefTextBoxFocus0f3cd5966f54143",
                "height": "40dp",
                "id": "fldPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "90dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0g6f133a2a34246",
                "text": "Password",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "231dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "defTextBoxPlaceholder",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var chkAgreement = new kony.ui.CheckBoxGroup({
                "height": "32dp",
                "id": "chkAgreement",
                "isVisible": true,
                "left": "88dp",
                "masterData": [
                    ["cbg1", "I agree with"]
                ],
                "selectedKeyValues": [
                    ["cbg1", "I agree with"]
                ],
                "selectedKeys": ["cbg1"],
                "skin": "CopyslCheckBoxGroup0b30f3180e5e748",
                "top": "342dp",
                "width": "29.63%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtTerms = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtTerms",
                "isVisible": true,
                "left": "212dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0e8302e265d4545",
                "text": "Terms of Usage",
                "top": "342dp",
                "width": "28.70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dtnSignUp = new kony.ui.Button({
                "centerX": "50.03%",
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "dtnSignUp",
                "isVisible": true,
                "left": "90dp",
                "skin": "CopydefBtnNormal0c69f65aa8d4047",
                "text": "SIGN UP",
                "top": "469dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtisRegistered = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtisRegistered",
                "isVisible": true,
                "left": "162dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0f6c6d5e177b145",
                "text": "I have",
                "top": "531dp",
                "width": "12.04%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtisRegistered2 = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtisRegistered2",
                "isVisible": true,
                "left": "212dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0a404fe12894947",
                "text": "an Account",
                "top": "531dp",
                "width": "21.27%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fldCity = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50.00%",
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "fldCity",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "77dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0f6497e685ff54b",
                "text": "City",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "285dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "defTextBoxPlaceholder",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var fldEmail = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50.00%",
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "fldEmail",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "77dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0g7d606ae161848",
                "text": "E mail",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "177dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "defTextBoxPlaceholder",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            this.add(fldPassword, chkAgreement, txtTerms, dtnSignUp, txtisRegistered, txtisRegistered2, fldCity, fldEmail);
        };
        return [{
            "addWidgets": addWidgetsfrmSignUp,
            "enabledForIdleTimeout": false,
            "id": "frmSignUp",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0cd5882a2211c4c"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});