define("frmChosenCinema", function() {
    return function(controller) {
        function addWidgetsfrmChosenCinema() {
            this.setDefaultUnit(kony.flex.DP);
            var contChosenCinema = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "712dp",
                "horizontalScrollIndicator": true,
                "id": "contChosenCinema",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "4dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            contChosenCinema.setDefaultUnit(kony.flex.DP);
            var txtCinemaTitle = new kony.ui.RichText({
                "centerX": "49.97%",
                "height": "35dp",
                "id": "txtCinemaTitle",
                "isVisible": true,
                "left": "19dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0f9f33836301344",
                "text": "Мультиплекс Караван",
                "top": "242dp",
                "width": "62.04%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnScreening = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "defBtnFocus",
                "height": "85dp",
                "id": "btnScreening",
                "isVisible": true,
                "left": "48dp",
                "skin": "CopydefBtnNormal0ed587e62f1bd46",
                "text": "Screenings",
                "top": "358dp",
                "width": "377dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtCinemaInfo = new kony.ui.RichText({
                "height": "42dp",
                "id": "txtCinemaInfo",
                "isVisible": true,
                "left": "19dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0be44449407384e",
                "text": "ул. Нижнеднепровская, 17\n\nДнепр",
                "top": "462dp",
                "width": "75.93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtCinemaPhone = new kony.ui.RichText({
                "height": "38dp",
                "id": "txtCinemaPhone",
                "isVisible": true,
                "left": "19dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0ce1d1fd160bf42",
                "text": "Телефон: 0800 505 333",
                "top": "488dp",
                "width": "62.04%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAdd = new kony.ui.RichText({
                "centerX": "50%",
                "height": "54dp",
                "id": "txtAdd",
                "isVisible": true,
                "left": "124dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0gb28e1204fda43",
                "text": "Кинотеатр ☆ MULTIPLEX ☆ в ТРЦ \"Караван\", Днепр ✓ Комфортные залы ✓ Выгодные цены.",
                "top": "277dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            kony.mvc.registry.add('FBox0c6c3e41513ff44', 'FBox0c6c3e41513ff44', 'FBox0c6c3e41513ff44Controller');
            var sgmCinema = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50.00%",
                "data": [{
                    "Image0h11c298bc93649": "multiplexkaravan.png"
                }, {
                    "Image0h11c298bc93649": "multiplexkaravan2.png"
                }, {
                    "Image0h11c298bc93649": "multiplexkaravan3.png"
                }, {
                    "Image0h11c298bc93649": "multiplexkaravan4.png"
                }],
                "groupCells": false,
                "height": "223dp",
                "id": "sgmCinema",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0f999d8e84b0f44",
                "rowTemplate": "FBox0c6c3e41513ff44",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_PAGEVIEW,
                "widgetDataMap": {
                    "Image0h11c298bc93649": "Image0h11c298bc93649"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            contChosenCinema.add(txtCinemaTitle, btnScreening, txtCinemaInfo, txtCinemaPhone, txtAdd, sgmCinema);
            this.add(contChosenCinema);
        };
        return [{
            "addWidgets": addWidgetsfrmChosenCinema,
            "enabledForIdleTimeout": false,
            "id": "frmChosenCinema",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0i378a42a0fcd43"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});